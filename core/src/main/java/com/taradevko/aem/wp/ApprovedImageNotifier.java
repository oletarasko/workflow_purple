/*
 Copyright Oleksandr Tarasenko

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 */
package com.taradevko.aem.wp;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;

@Component(immediate = true, property = {"process.label=WP: Notify user about approved image"})
public class ApprovedImageNotifier implements WorkflowProcess {

	private static final Logger LOG = LoggerFactory.getLogger(ApprovedImageNotifier.class);

	@Override
	public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
			final MetaDataMap metaDataMap) throws WorkflowException {
		LOG.info("User will be notified that his message was approved.");
	}
}
