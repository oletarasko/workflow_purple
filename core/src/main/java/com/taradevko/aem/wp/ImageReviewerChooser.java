/*
 Copyright Oleksandr Tarasenko

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 */
package com.taradevko.aem.wp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.payload.PayloadInfo;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.ParticipantStepChooser;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.metadata.MetaDataMap;

@Component(immediate = true, property = {"chooser.label=WP: Image reviewer chooser"})
public class ImageReviewerChooser implements ParticipantStepChooser {

    private static final Logger  LOG                       = LoggerFactory.getLogger(ImageReviewerChooser.class);
    private static final String  USER_ADMIN                = "admin";
    private static final String  GROUP_USER_IMAGE_REVIEWER = "user-imagereviewer-";
    private final        Pattern pattern                   = Pattern.compile("/content/dam/we-retail/(\\w{2})/.*");

    @Override
    public String getParticipant(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap)
            throws WorkflowException {

        String imageReviewer = USER_ADMIN;
        WorkflowData workflowData = workItem.getWorkflowData();
        if (PayloadInfo.PAYLOAD_TYPE.JCR_PATH.toString().equals(workflowData.getPayloadType())
                && workflowData.getPayload() != null) {

            String payloadPath = (String) workflowData.getPayload();
            Matcher matcher = pattern.matcher(payloadPath);
            if (matcher.find()) {
                String market = matcher.group(1);
                imageReviewer = GROUP_USER_IMAGE_REVIEWER + market;
            }
        }
        LOG.info("###ImageReviewerChooser: {} selected to review image", imageReviewer);
        return imageReviewer;
    }
}
